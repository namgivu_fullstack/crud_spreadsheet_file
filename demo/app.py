if __name__ == '__main__':
  def demo230103_1544_basicread():
    # file path
    from pathlib import Path
    SH = Path(__file__).resolve().parent
    p  = SH / 'sample.xlsx' ; assert p.is_file() ; xls_f=str(p)

    # load workbook+worksheet
    from openpyxl import load_workbook
    wb = load_workbook(filename=xls_f)
    ws = wb.active

    #region read cell
    print(f'''
  {ws.max_row=}    
  {ws.max_column=}   
  
  {ws.cell(1,          1            )=} 
  {ws.cell(ws.max_row, ws.max_column)=} 
  
  {ws.cell(1,          1            ).value=} 
  {ws.cell(ws.max_row, ws.max_column).value=} 
    '''.strip()+'\n')
    #endregion read cell

    #region print all cells
    print()

    n_row = ws.max_row     # n_xx aka number_of_xx
    n_col = ws.max_column  # n_xx aka number_of_xx
    for r in range(1, n_row+1):
        for c in range(1, n_col+1):
            print(f'{r},{c}:{ws.cell(r,c).value}'.ljust(44), end=' ')
        print()
    #endregion print all cells

    #region header + value row
    print()
    row_1 = ws[1]

    n_row = ws.max_row  # n_xx aka number_of_xx
    row_n = ws[n_row]

    print('--- header row')
    print(f'{[str(c.value).ljust(22) for c in row_1]}')
    print(f'{[str(c.value).ljust(22) for c in row_n]}')

    print('--- value row')
    for r in range(1, n_row+1):
        print(f'{[str(c.value).ljust(33) for c in ws[r] ]}')
    #endregion header + value row

  def demo230103_1600_printfilteredrow():
    print()

    # file path
    from pathlib import Path
    SH = Path(__file__).resolve().parent
    p  = SH / 'sample_filtered.xlsx' ; assert p.is_file() ; xls_f=str(p)

    # load workbook+worksheet
    from openpyxl import load_workbook
    wb = load_workbook(filename=xls_f)
    ws = wb.active

    print('--- header row')
    print(f'{[str(c.value).ljust(22) for c in ws[1]]}')

    print('--- value row')
    for r in range(1, ws.max_row+1):
        hidden = ws.row_dimensions[r].hidden  # check if row :r is filtered out ref. https://stackoverflow.com/q/41824257
        if not hidden: print(f'{[str(c.value).ljust(33) for c in ws[r] ]}')

  def demo230103_1606_setfiltervalue():
    from openpyxl import load_workbook

    # file path
    from pathlib import Path
    SH = Path(__file__).resolve().parent
    p  = SH /        'sample.xlsx'  ; assert p.is_file() ; nofilter_f=str(p)
    p  = SH / 'zoutput_allchecked.sample.xlsx' ;           withfilter_allchecked_f=str(p)
    p  = SH / 'zoutput_onlyspecificchecked.sample.xlsx' ;  withfilter_onlyspecificchecked_f=str(p)

    '''
    we will try to set the filter checkbox, at col :phuong ie colindex=3, to be 'Phường 12'
    ref. https://stackoverflow.com/a/66377091
    '''
    def print_filtered_row(filename):
        wb = load_workbook(filename)
        ws = wb.active

        for r in range(1, ws.max_row+1):
            hidden = ws.row_dimensions[r].hidden  # check if row :r is filtered out ref. https://stackoverflow.com/q/41824257
            if not hidden: print(f'{[str(c.value).ljust(33) for c in ws[r] ]}')

    print('\n--- no autofilter') ; print_filtered_row(nofilter_f)

    #region turn on autofilter, but leave all checkboxes checked as default
    wb=load_workbook(nofilter_f) ; ws=wb.active

    ws.auto_filter.ref = ws.dimensions

    wb.save(withfilter_allchecked_f)

    print('\n--- with autofilter, but all checked') ; print_filtered_row(withfilter_allchecked_f)
    #endregion turn on autofilter, but leave all checkboxes checked as default

    #region turn on autofilter, check only specific value

    # wb=load_workbook(nofilter_f) ; ws=wb.active             # start from no autofilter
    wb=load_workbook(withfilter_allchecked_f) ; ws=wb.active  # start from autofilter w/ all checked
    #                                                         # start with either above, will have same result

    ws.auto_filter.ref = ws.dimensions
    ws.auto_filter.add_filter_column(2, ['Phường 01', 'Phường 02', 'Phường 12'])  # filter col 2, at this specific phuong only
    #                                   #FIXME this help check the checkbox, BUT rows not filtered/applied ref. https://stackoverflow.com/a/42556925 https://stackoverflow.com/a/34587903

    wb.save(withfilter_onlyspecificchecked_f)

    print('\n--- with autofilter, but all checked') ; print_filtered_row(withfilter_onlyspecificchecked_f)
    #endregion turn on autofilter, check only specific value

  demo230103_1544_basicread()
  demo230103_1600_printfilteredrow()
  demo230103_1606_setfiltervalue()
