from pathlib import Path
from openpyxl.reader.excel import load_workbook
from copy import copy


#region helper
def copy_row(sheet, fr_rowidx, to_rowidx):
  """
  ref https://stackoverflow.com/a/75411919/248616
  """
  for c in range(1, sheet.max_column + 1):
    fr = sheet.cell(row=fr_rowidx, column=c)
    to = sheet.cell(row=to_rowidx, column=c)

    'copy value'
    to.value = fr.value

    'copy style'
    to.font          = copy(fr.font)
    to.alignment     = copy(fr.alignment)
    to.border        = copy(fr.border)
    to.fill          = copy(fr.fill)
    to.number_format = fr.number_format


def fill_val(sheet, at_row, values):
  for c,v in enumerate(values, start=1):
    sheet.cell(row=at_row,column=c).value = v
#endregion helpers

if __name__ == '__main__':
  def demo240600_demo_filldata2template():
    xlsx_template_p = Path(__file__).parent / 'template.xlsx'
    xlsx_output_p   = Path(__file__).parent / 'zo-output.xlsx'

    wb = load_workbook(xlsx_template_p)

    sheet_o = wb['template']

    'fill data into cloned row to have styled'  #region
    row_list = [
      ['22/11/00', 'A122', '4444', 'JJJ', 'PIC', 'SSS', 'DDD', '07:09', '09:49', '00:00', '02:35', '00:00', '00:00', 'SS', '', '', '', '', '', '', '', '', '', '', '', 's'],
    ]

    template_row    = 4
    datarow_startat = 5

    for i,values in enumerate(row_list, start=datarow_startat):
      'clone templaterow'; sheet_o.insert_rows(           i); copy_row(sheet=sheet_o, fr_rowidx=template_row, to_rowidx=i)
      'fill data';         fill_val(sheet=sheet_o, at_row=i, values=values)
    'fill data into cloned row to have styled'  #endregion

    'rm template row'; sheet_o.delete_rows(idx=template_row, amount=1)
    wb.save(xlsx_output_p)

  demo240600_demo_filldata2template()
